# Experiments with Actions

This is a dummy repository for me to play around with GitHub Actions and other
CI automation.  Other than this file, the only interesting content are the
config files for the various CI stacks.
